#!/usr/bin/env groovy
package com.example

class Docker implements Serializable {

    def script

    Docker(script) {
        this.script=script
    }

    def buildDockerImage(String imageName) {
        script.echo "Building docker image to docker repository..."
            script.sh "docker build -t $imageName ."
    }

    def dockerLogin() {
        script.withCredentials([
                script.usernamePassword(
                        credentialsId: 'docker-hub-credentials',
                        usernameVariable: 'USER',
                        passwordVariable: 'PASSWORD')]) {
            script.sh "echo $script.PASSWORD | docker login -u $script.USER --password-stdin"
        }
    }

    def dockerPush(String imageName) {
        script.sh "docker push $imageName"
    }

}