#!/usr/bin/env groovy

def call() {
    echo "Building jar for branch $BRANCH_NAME"
    sh 'mvn package'
}