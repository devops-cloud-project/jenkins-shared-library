#!/usr/bin/env groovy

def buildJar() {
    echo "Building jar for branch $BRANCH_NAME"
    sh 'mvn package'
}

def buildImage(String imageName){
    echo "Building docker image to docker repository..."
    sh "docker build -t $imageName ."
}


def dockerLogin() {
    withCredentials([
            usernamePassword(
                    credentialsId: 'docker-hub-credentials',
                    usernameVariable: 'USER',
                    passwordVariable: 'PASSWORD')]) {
        sh "echo $PASSWORD | docker login -u $USER --password-stdin"
    }
}

def dockerPush(String imageName) {
    sh "docker push $imageName"
}

def deployToEc2(String imageName) {

    def shellCmd = "bash ./server-cmds.sh $imageName"
    def ec2Instance = "ec2-user@18.134.142.64"

    // 'ec2-server-credentials' - created under this name in Jenkins credentials for specific pipeline
    sshagent(['ec2-server-credentials']) {
        //copy server-cmds.sh
        sh "scp server-cmds.sh $ec2Instance:/home/ec2-user/server-cmds.sh"

        //copy docker-compose.yaml
        sh "scp docker-compose.yaml $ec2Instance:/home/ec2-user/docker-compose.yaml"

        sh "ssh -o StrictHostKeyChecking=no $ec2Instance $shellCmd"
    }
}

def incrementVersion() {
    sh "mvn build-helper:parse-version versions:set \
                        -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} versions:commit"
    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
    //If we want to append build number as SUFFIX
    def version = matcher[0][1]
    return env.IMAGE_NAME = "adik0802/demo-app:$version-$BUILD_NUMBER"
}

def commitVersion() {
    withCredentials([
            usernamePassword(
                    credentialsId: 'gitlab-credentials',
                    usernameVariable: 'USER',
                    passwordVariable: 'PASSWORD')]) {
        sh 'git config --global user.email "any-email-name@example.com"'
        sh 'git config --global user.name "jenkins"'

        sh 'git status'
        sh 'git branch'
        sh 'git config --list'

        sh "git remote set-url origin https://${USER}:${PASSWORD}@gitlab.com/devops-cloud-project/09-multiple-branches-java-maven-app.git"
        sh 'git add .'
        sh 'git commit -m "Jenkins: version increment"'
        sh 'git push origin HEAD:master'
    }
}

